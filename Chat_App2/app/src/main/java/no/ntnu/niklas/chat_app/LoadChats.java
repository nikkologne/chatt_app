package no.ntnu.niklas.chat_app;



import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static android.R.attr.id;

/**
 * Created by niklas on 10/4/17.
 */

public class LoadChats extends AsyncTask<URL,Integer,List<Chat>> {

    public interface OnPostExecute {
        void onPostExecute(List<Chat> photos);
    }

    OnPostExecute callback;

    public LoadChats(OnPostExecute callback) {
        this.callback = callback;
    }

    @Override
    protected List<Chat> doInBackground(URL... urls) {
        if(urls.length < 1) return Collections.EMPTY_LIST;


        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");List<Chat> result = new ArrayList<>();

        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection)urls[0].openConnection();
            JsonReader jr = new JsonReader(new InputStreamReader(con.getInputStream()));
            jr.beginArray();
            while (jr.hasNext()) {
                String name = null;
                long id = -1;

                jr.beginObject();
                while (jr.hasNext()) {
                    switch (jr.nextName()) {
                        case "name":
                            name = jr.nextString();
                            break;
                        case "id":
                            id = jr.nextLong();
                            break;

                        default:
                            jr.skipValue();
                    }
                }
                jr.endObject();
                result.add(new Chat(id, name));
            }
            jr.endArray();
        } catch (IOException e) {
            Log.e("LoadThumb","Failed to load photos from " + urls[0],e);
        }


        return result;
    }

    @Override
    protected void onPostExecute(List<Chat> photos) {
        if(callback != null)
            callback.onPostExecute(photos);
    }
}
