package no.ntnu.niklas.chat_app;

import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by niklas on 10/11/17.
 */

public class GetMessages extends AsyncTask<URL,Integer,List<Message>>{

    public interface OnPostExecute {
        void onPostExecute(List<Message> messages);
    }

    GetMessages.OnPostExecute callback;

    public GetMessages(GetMessages.OnPostExecute callback) {
        this.callback = callback;
    }

    @Override
    protected List<Message> doInBackground(URL... urls) {
        if(urls.length < 1) return Collections.EMPTY_LIST;


        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");List<Message> result = new ArrayList<>();

        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection)urls[0].openConnection();
            JsonReader jr = new JsonReader(new InputStreamReader(con.getInputStream()));
            jr.beginArray();
            while (jr.hasNext()) {
                String name = null;
                String message = null;
                Long messageId = null;
                long cid = -1;

                jr.beginObject();
                while (jr.hasNext()) {
                    switch (jr.nextName()) {
                        case "messageId":
                            messageId = jr.nextLong();
                            break;
                        case "message":
                            message = jr.nextString();
                            break;
                        case "name":
                            name = jr.nextString();
                            break;
                        case "cid":
                            cid = jr.nextLong();
                            break;


                        default:
                            jr.skipValue();
                    }
                }
                jr.endObject();
                result.add(new Message(messageId,message, name, cid));
            }
            jr.endArray();
        } catch (IOException e) {
            Log.e("LoadThumb","Failed to load photos from " + urls[0],e);
        }


        return result;
    }
    @Override
    protected void onPostExecute(List<Message> Messages) {
        if(callback != null)
            callback.onPostExecute(Messages);
    }


}
