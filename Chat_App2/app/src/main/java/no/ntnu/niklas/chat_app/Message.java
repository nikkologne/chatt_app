package no.ntnu.niklas.chat_app;

/**
 * Created by niklas on 10/11/17.
 */

public class Message {
    long messageId;
    String message;
    String name;
    Long cid;

    public Message(Long messageId, String message, String name, long cid) {
        this.messageId = messageId;
        this.message = message;
        this.name = name;
        this.cid = cid;

    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }
}
