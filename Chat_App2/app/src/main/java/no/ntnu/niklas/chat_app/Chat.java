package no.ntnu.niklas.chat_app;

/**
 * Created by niklas on 10/4/17.
 */

class Chat {
    long id;
    String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Chat(long id, String name) {

        this.id = id;
        this.name = name;
    }
}
