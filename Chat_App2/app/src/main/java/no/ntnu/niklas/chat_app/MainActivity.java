package no.ntnu.niklas.chat_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    public void showChats(View view){

        try {
            new LoadChats(new LoadChats.OnPostExecute() {
                @Override
                public void onPostExecute(List<Chat> chats) {
                    for(Chat p : chats) {
                        System.out.println("Chat: " + p.getId());
                    }
                }
            }).execute(new URL("http://158.38.140.184:8080/pstore/api/store/images/"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


    }
}
